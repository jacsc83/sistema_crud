<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlumnosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('alumnos')->insert([
            'nombre' => 'Juan',
            'apellido_paterno' => 'Cuerdas',
            'apellido_materno' => 'De Leon',
            'semestre' => 1,
            'grupo' => 'A',
        ]);

    }
}
